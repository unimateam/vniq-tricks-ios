//
//  MainViewController.swift
//  VietNamIQTricks
//
//  Created by Linh NGUYEN on 6/04/18.
//  Copyright © 2017 Linh NGUYEN. All rights reserved.
//

import UIKit
import SwiftyJSON
import MagicalRecord

class MainViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    var items: [CDQuestion] = [CDQuestion]()
    var filteredItems = [CDQuestion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "VietNamIQTricks"
        
        // notification
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(importedData), name: Notification.Name("ImportedData"), object: nil)
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = ""
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        fetchData()
    }
    
    @objc func importedData() {
        DispatchQueue.main.async {
            self.fetchData()
        }
    }
    
    func fetchData() {
        items = CDQuestion.mr_findAllSorted(by: "question", ascending: true) as! [CDQuestion]
        DispatchQueue.main.async {
            self.title = "\(self.items.count) questions"
            self.tableView.reloadData()
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
}

extension MainViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "ResultCell")
        }
        let question: CDQuestion
        if isFiltering() {
            question = filteredItems[indexPath.row]
        } else {
            question = items[indexPath.row]
        }
        cell!.textLabel?.text = question.question
        cell!.detailTextLabel?.text = question.desc!
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredItems.count
        }
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let question: CDQuestion
        if isFiltering() {
            question = filteredItems[indexPath.row]
        } else {
            question = items[indexPath.row]
        }
        let message = String("\(question.question!) - \(question.answer1!) - \(question.answer2!) - \(question.answer3!) - \(question.answer4!) - Correct: \(question.correctAnswer)")
        let alert = UIAlertController(title: "VietNamIQTricks", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension MainViewController: UISearchResultsUpdating, UISearchBarDelegate {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(MainViewController.filterContentForSearchText(_:)), object: searchController.searchBar.text!)
        self.perform(#selector(MainViewController.filterContentForSearchText(_:)), with: searchController.searchBar.text!, afterDelay: 0.5)
    }
    
    @objc func filterContentForSearchText(_ searchText: String) {
        filteredItems = items.filter({( question : CDQuestion) -> Bool in
            if searchBarIsEmpty() {
                return true
            } else {
                let searchTextIn = question.searchText!.lowercased()
                let keywords = searchText.lowercased().components(separatedBy: "+")
                for key in keywords {
                    let trimmed = key.trimmingCharacters(in: .whitespacesAndNewlines)
                    if (!searchTextIn.contains(trimmed)) {
                        return false
                    }
                }
                return true
            }
        })
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(MainViewController.filterContentForSearchText(_:)), object: searchController.searchBar.text!)
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
