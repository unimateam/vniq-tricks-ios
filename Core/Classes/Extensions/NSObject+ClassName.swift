//
//  NSObject+ClassName.swift
//  VietNamIQTricks
//
//  Created by Linh NGUYEN on 6/04/18.
//  Copyright © 2017 Linh NGUYEN. All rights reserved.
//

import Foundation

public extension NSObject {
    
    public class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    public var className: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
    
}
