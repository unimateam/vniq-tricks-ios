//
//  DBMigrationService.swift
//  VietNamIQTricks
//
//  Created by Linh NGUYEN on 6/4/18.
//  Copyright © 2018 Linh NGUYEN. All rights reserved.
//

import UIKit
import SQLite
import MagicalRecord

typealias DBMigrationCompletion = ((Bool) -> Void)

class DBMigrationService: NSObject {
    
    var completion : DBMigrationCompletion?
    var importingContext = NSManagedObjectContext.mr_()
    var importingQueue = OperationQueue()
    
    func startImporting() {
        importingQueue.maxConcurrentOperationCount = 1
        // Import Data 1
        importingQueue.addOperation {
            self.parseData1(completion: { (success) in
                print("Imported Data 1: ", success)
            })
        }
        
        // Import Data 2
        importingQueue.addOperation {
            self.parseData2(completion: { (success) in
                print("Imported Data 2: ", success)
            })
        }
        
        // Import Data 3
        importingQueue.addOperation {
            self.parseData3(completion: { (success) in
                print("Imported Data 3: ", success)
            })
        }
        
        // Import Data 4
        importingQueue.addOperation {
            self.parseData4(completion: { (success) in
                print("Imported Data 4: ", success)
                self.completion?(success)
            })
        }
    }
    
    func parseData1(completion: DBMigrationCompletion?) {
        do {
            let db = try Connection(Bundle.main.path(forResource: "data1", ofType: "sqlite")!)
            for i in 1...15 {
                let questions1 = Table(String("Questions\(i)"))
                let clQuestion = Expression<String?>("Question")
                let clAnswerOne = Expression<String?>("AnswerOne")
                let clAnswerTwo = Expression<String?>("AnswerTwo")
                let clAnswerThree = Expression<String?>("AnswerThree")
                let clAnswerFour = Expression<String?>("AnswerFour")
                let clCorrectAnswer = Expression<Int?>("CorrectAnswer")
                for question in try db.prepare(questions1) {
                    if let textQuestion = question[clQuestion],
                        let textAnswer1 = question[clAnswerOne],
                        let textAnswer2 = question[clAnswerTwo],
                        let textAnswer3 = question[clAnswerThree],
                        let textAnswer4 = question[clAnswerFour],
                        let textCorrectAnswer = question[clCorrectAnswer] {
                        self.saveData(textQuestion: textQuestion, textAnswer1: textAnswer1, textAnswer2: textAnswer2, textAnswer3: textAnswer3, textAnswer4: textAnswer4, textCorrectAnswer: textCorrectAnswer)
                    }
                }
            }
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(true)
        } catch {
            debugPrint(error.localizedDescription)
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(false)
        }
    }
    
    func parseData2(completion: DBMigrationCompletion?) {
        do {
            let db = try Connection(Bundle.main.path(forResource: "data2", ofType: "sqlite")!)
            let questions1 = Table(String("Question"))
            let clQuestion = Expression<String?>("question")
            let clAnswerOne = Expression<String?>("casea")
            let clAnswerTwo = Expression<String?>("caseb")
            let clAnswerThree = Expression<String?>("casec")
            let clAnswerFour = Expression<String?>("cased")
            let clCorrectAnswer = Expression<Int?>("truecase")
            for question in try db.prepare(questions1) {
                if let textQuestion = question[clQuestion],
                    let textAnswer1 = question[clAnswerOne],
                    let textAnswer2 = question[clAnswerTwo],
                    let textAnswer3 = question[clAnswerThree],
                    let textAnswer4 = question[clAnswerFour],
                    let textCorrectAnswer = question[clCorrectAnswer] {
                    self.saveData(textQuestion: textQuestion, textAnswer1: textAnswer1, textAnswer2: textAnswer2, textAnswer3: textAnswer3, textAnswer4: textAnswer4, textCorrectAnswer: textCorrectAnswer)
                }
            }
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(true)
        } catch {
            debugPrint(error.localizedDescription)
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(false)
        }
    }
    
    func parseData3(completion: DBMigrationCompletion?) {
        do {
            let db = try Connection(Bundle.main.path(forResource: "data3", ofType: "sqlite")!)
            let questions1 = Table(String("question_tb"))
            let clQuestion = Expression<String?>("q_content")
            let clAnswerOne = Expression<String?>("casea")
            let clAnswerTwo = Expression<String?>("caseb")
            let clAnswerThree = Expression<String?>("casec")
            let clAnswerFour = Expression<String?>("cased")
            for question in try db.prepare(questions1) {
                if let textQuestion = question[clQuestion],
                    let textAnswer1 = question[clAnswerOne],
                    let textAnswer2 = question[clAnswerTwo],
                    let textAnswer3 = question[clAnswerThree],
                    let textAnswer4 = question[clAnswerFour]{
                    let textCorrectAnswer = Int(1)
                    self.saveData(textQuestion: textQuestion, textAnswer1: textAnswer1, textAnswer2: textAnswer2, textAnswer3: textAnswer3, textAnswer4: textAnswer4, textCorrectAnswer: textCorrectAnswer)
                }
            }
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(true)
        } catch {
            debugPrint(error.localizedDescription)
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(false)
        }
    }
    
    func parseData4(completion: DBMigrationCompletion?) {
        do {
            let db = try Connection(Bundle.main.path(forResource: "data4", ofType: "sqlite")!)
            let questions1 = Table(String("QuestionDetail"))
            let clQuestion = Expression<String?>("question")
            let clAnswerOne = Expression<String?>("casea")
            let clAnswerTwo = Expression<String?>("caseb")
            let clAnswerThree = Expression<String?>("casec")
            let clAnswerFour = Expression<String?>("cased")
            for question in try db.prepare(questions1) {
                if let textQuestion = question[clQuestion],
                    let textAnswer1 = question[clAnswerOne],
                    let textAnswer2 = question[clAnswerTwo],
                    let textAnswer3 = question[clAnswerThree],
                    let textAnswer4 = question[clAnswerFour]{
                    let textCorrectAnswer = Int(1)
                    self.saveData(textQuestion: textQuestion, textAnswer1: textAnswer1, textAnswer2: textAnswer2, textAnswer3: textAnswer3, textAnswer4: textAnswer4, textCorrectAnswer: textCorrectAnswer)
                }
            }
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(true)
        } catch {
            debugPrint(error.localizedDescription)
            self.importingContext.mr_saveToPersistentStoreAndWait()
            completion?(false)
        }
    }
    
    fileprivate func saveData(textQuestion: String, textAnswer1: String, textAnswer2: String,
                              textAnswer3: String, textAnswer4: String, textCorrectAnswer: Int) {
        let processedQuestion = self.processString(inputString: textQuestion)
        let processedAnswer1 = self.processString(inputString: textAnswer1)
        let processedAnswer2 = self.processString(inputString: textAnswer2)
        let processedAnswer3 = self.processString(inputString: textAnswer3)
        let processedAnswer4 = self.processString(inputString: textAnswer4)
        
        let predicate = NSPredicate(format: "question = %@ OR (answer1 = %@ AND answer2 = %@ AND answer3 = %@)", processedQuestion, processedAnswer1, processedAnswer2, processedAnswer3)
        if CDQuestion.mr_countOfEntities(with: predicate, in: self.importingContext) > 0 {
            // what should I do?
        } else {
            if let cdQuestion = CDQuestion.mr_createEntity(in: self.importingContext) {
                cdQuestion.question =  processedQuestion
                cdQuestion.answer1 =  processedAnswer1
                cdQuestion.answer2 =  processedAnswer2
                cdQuestion.answer3 =  processedAnswer3
                cdQuestion.answer4 =  processedAnswer4
                cdQuestion.correctAnswer =  Int16(textCorrectAnswer)
                var array = [String]()
                if (cdQuestion.correctAnswer == 1) {
                    array.append(cdQuestion.answer1!)
                    array.append(cdQuestion.answer2!)
                    array.append(cdQuestion.answer3!)
                    array.append(cdQuestion.answer4!)
                } else if (cdQuestion.correctAnswer == 2) {
                    array.append(cdQuestion.answer2!)
                    array.append(cdQuestion.answer1!)
                    array.append(cdQuestion.answer3!)
                    array.append(cdQuestion.answer4!)
                } else if (cdQuestion.correctAnswer == 3) {
                    array.append(cdQuestion.answer3!)
                    array.append(cdQuestion.answer1!)
                    array.append(cdQuestion.answer2!)
                    array.append(cdQuestion.answer4!)
                } else if (cdQuestion.correctAnswer == 4) {
                    array.append(cdQuestion.answer4!)
                    array.append(cdQuestion.answer1!)
                    array.append(cdQuestion.answer2!)
                    array.append(cdQuestion.answer3!)
                }
                cdQuestion.desc = array.joined(separator: " - ")
                var searchText = [String]()
                searchText.append(cdQuestion.question!.folding(options: .diacriticInsensitive, locale: .current))
                searchText.append(cdQuestion.answer1!.folding(options: .diacriticInsensitive, locale: .current))
                searchText.append(cdQuestion.answer2!.folding(options: .diacriticInsensitive, locale: .current))
                searchText.append(cdQuestion.answer3!.folding(options: .diacriticInsensitive, locale: .current))
                searchText.append(cdQuestion.answer4!.folding(options: .diacriticInsensitive, locale: .current))
                let joinedText = searchText.joined(separator: " - ").lowercased()
                cdQuestion.searchText = joinedText.replacingOccurrences(of: "đ", with: "d")
            }
        }
    }
    
    fileprivate func processString(inputString: String) -> String {
        let trimmed = inputString.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed
    }
}
