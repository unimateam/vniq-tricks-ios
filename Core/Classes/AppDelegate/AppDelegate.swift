//
//  AppDelegate.swift
//  VietNamIQTricks
//
//  Created by Linh NGUYEN on 6/04/18.
//  Copyright © 2017 Linh NGUYEN. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var backgroundSessionCompletionHandler : (() -> Void)?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainVC = MainViewController(nibName: MainViewController.className, bundle: nil)
        let mainNAV = UINavigationController(rootViewController: mainVC)
        self.window?.rootViewController = mainNAV
        self.window?.makeKeyAndVisible()
        setup3rdSDKs()
        setupCoreData()
        //initData()
        return true
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }

    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[0]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: AppDefine.modelName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent(String(format: "%@.sqlite", AppDefine.modelName))
        
        if !FileManager.default.fileExists(atPath: url.path) {
            let sourceSqliteURLs = [Bundle.main.url(forResource: AppDefine.modelName, withExtension: "sqlite")!,
                                    Bundle.main.url(forResource: AppDefine.modelName, withExtension: "sqlite-wal")!,
                                    Bundle.main.url(forResource: AppDefine.modelName, withExtension: "sqlite-shm")!]
            
            let destSqliteURLs = [self.applicationDocumentsDirectory.appendingPathComponent(String(format: "%@.sqlite", AppDefine.modelName)),
                                  self.applicationDocumentsDirectory.appendingPathComponent(String(format: "%@.sqlite-wal", AppDefine.modelName)),
                                  self.applicationDocumentsDirectory.appendingPathComponent(String(format: "%@.sqlite-shm", AppDefine.modelName))]
            
            do{
                for i in 0..<sourceSqliteURLs.count{
                    try FileManager.default.copyItem(at: sourceSqliteURLs[i], to: destSqliteURLs[i])
                }
            } catch {
                print(error)
            }
            
        }
        
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            print("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
}

extension AppDelegate {
    
    fileprivate func setup3rdSDKs() {
        
    }
    
    fileprivate func setupCoreData() {
        MagicalRecord.enableShorthandMethods()
        NSPersistentStoreCoordinator.mr_setDefaultStoreCoordinator(persistentStoreCoordinator)
        NSManagedObjectContext.mr_initializeDefaultContext(with: persistentStoreCoordinator)
    }
    
    fileprivate func initData() {
        if CDQuestion.mr_countOfEntities() == 0 {
            SVProgressHUD.show(withStatus: "Importing data")
            let migrationService = DBMigrationService()
            migrationService.completion = { (success) in
                print("Importing DB: Done")
                OperationQueue.main.addOperation({
                    SVProgressHUD.showSuccess(withStatus: "Done")
                })
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name("ImportedData"), object: nil)
            }
            migrationService.startImporting()
        }
    }
}
